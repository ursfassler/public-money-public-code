# (C) Copyright 2018: Urs Fässler, www.bitzgi.ch
# SPDX-License-Identifier: CC-BY-SA-4.0

handname=handout
slidename=slide
pdfname=public-money-public-code

slidetex=$(slidename).tex
handouttex=$(handname).tex
slidepdf=$(pdfname)_$(slidename).pdf
handoutpdf=$(pdfname)_$(handname).pdf

gs-flags=-sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress -dNOPAUSE -dQUIET -dBATCH -dDetectDuplicateImages -dCompressFonts=true

merged: handout slide
	gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=$(pdfname).pdf $(handoutpdf) $(slidepdf)

handout:
	pdflatex -draftmode -halt-on-error $(handouttex) 1> /dev/null
	pdflatex -halt-on-error $(handouttex) 1> /dev/null
	gs $(gs-flags) -sOutputFile=$(handoutpdf) $(handname).pdf

slide:
	pdflatex -draftmode -halt-on-error $(slidetex) 1> /dev/null
	pdflatex -halt-on-error $(slidetex) 1> /dev/null
	gs $(gs-flags) -sOutputFile=$(slidepdf) $(slidename).pdf

debug:
	pdflatex $(handouttex)

clean:
	rm -f *.log *.toc *.aux *.bbl *.blg *.lof *.lot *.out *.bak *.nav *.snm *.vrb

clear: 
	rm -f *.dvi *.pdf *.ps

